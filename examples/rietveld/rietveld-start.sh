#!/bin/sh
cd $HOME/codereview/examples/rietveld || exit 1

pkill -u rietveld -f 'python.*manage.*runfcgi' && sleep 3
ulimit -v 400000
nohup setsid python2.6 manage.py runfcgi method=prefork maxrequests=10 socket=/home/rietveld/rietveld.sock daemonize=false </dev/null >>$HOME/rietveld.log 2>&1 &
(sleep 3  && chmod go+w /home/rietveld/rietveld.sock) &
(sleep 60 && chmod go+w /home/rietveld/rietveld.sock) &

#pkill -u rietveld -f 'python.*manage.*runserver' && sleep 3
#nohup setsid python2.6 manage.py runserver --noreload 0.0.0.0:8000 </dev/null >>$HOME/rietveld.log 2>&1 &
