#!/bin/sh

proj=$1
if [ "x$proj" = "x" ]; then
    echo Usage: rietveld-integrate.sh PROJECT
    exit 1
fi
export LC_CTYPE=en_US.UTF-8
unset http_proxy
set -e
cd $HOME/projects/$proj

repov=$(svn info -r HEAD|grep -E ^Revision:|cut -d\  -f2)
test -n "$repov" || exit 1
verfile=../$proj.svnversion
if [ -f $verfile ]; then
    localv=$(cat $verfile)
else
    localv=$(($(svnversion) - 1))
    echo $localv >$verfile
fi

if [ "$repov" -eq "$localv" ]; then
    #echo nothing to do
    exit 0
fi
echo $localv =\> $repov

for i in $(seq $localv $(($repov - 1))); do
    n=$(($i+1))
    echo -- $i -\> $n
    svnout=$(svn up -r $n)
    cat <<EOL
$svnout
EOL
    svnlines=$(wc -l <<EOL
$svnout
EOL
)
    if [ $svnlines -gt 80 ]; then
        echo "$svnlines changes in update, skipping revision"
        continue
    fi
    desc="$(svn log -c $n)"
    msg="$proj r$n"
#    msg="$proj r$n $((cat | head -4 | tail -1) <<EOM
#$desc
#EOM
#)"
    $HOME/bin/rietveld-upload.py -s review.alise.lv:80 -m "$msg" -d "$desc" --rev $i:$n
    echo $n >$verfile
done
